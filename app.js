(function() {
	var app = angular.module('application', []);

	app.directive('navBar', function(){
		return {
			restrict: 'E',
			templateUrl: 'Views/_navBar.html',
			controller: function(){
				this.tab = 'home';

				this.selectTab = function(setTab){
					this.tab = setTab;
				};

				this.isSelected = function(checkTab){
					return this.tab === checkTab;
				};
			},
			controllerAs: 'panel'
		};
	});

	app.controller('AppController', ['$scope', function($scope){
		/****************Dependencies*************************/
		var fs = require('fs');
		var gui = require('nw.gui');
		/*****************************************************/

		gui.Window.get().maximize();

		/****************Setup Editor**************************/
		var editor = ace.edit("editor");
		editor.$blockScrolling = Infinity;
		editor.getSession().setMode("ace/mode/markdown");

		editor.getSession().on("change", function() {
			$scope.$apply(function(){
				$scope.document.saved = false;
			});
			var md_content = editor.getSession().getValue();
			window.localStorage["md_data"] = md_content;
			var html_content = markdown.toHTML(md_content);
			$("#preview").html(html_content);

			MathJax.Hub.Queue(["Typeset", MathJax.Hub, "preview"]);
		});
		/**********************************************************/

		/*********************Setup keys***************************/
		var options = [];

		options.push({
			key : "Ctrl+n",
			active : function() {
				console.log("Global desktop keyboard shortcut: " + this.key + " active."); 
				$scope.clearEditor();
			},
			failed : function(msg) {
	    			// :(, fail to register the |key| or couldn't parse the |key|.
	    				console.log(msg + "; " + this.key);
	    			}
	    		});
		options.push({
			key : "Ctrl+s",
			active : function() {
				console.log("Global desktop keyboard shortcut: " + this.key + " active.");
				$scope.saveFile(); 
			},
			failed : function(msg) {
	    			// :(, fail to register the |key| or couldn't parse the |key|.
	    				console.log(msg + "; " + this.key);
	    			}
	    		});
		options.push({
			key : "Ctrl+o",
			active : function() {
				console.log("Global desktop keyboard shortcut: " + this.key + " active.");
				$scope.chooseFile(); 
			},
			failed : function(msg) {
	    			// :(, fail to register the |key| or couldn't parse the |key|.
	    				console.log(msg + "; " + this.key);
	    			}
	    		});
		

		var win = gui.Window.get();

		options.forEach(function(option){
			// Create a shortcut with |option|.
			var shortcut = new gui.Shortcut(option);

			// Register global desktop shortcut, which can work without focus.
			gui.App.unregisterGlobalHotKey(shortcut)
			gui.App.registerGlobalHotKey(shortcut);
		});

		// Listen to the focus event
		win.on('focus', function() {
			options.forEach(function(option){
				// Create a shortcut with |option|.
				var shortcut = new gui.Shortcut(option);

				// Register global desktop shortcut, which can work without focus.
				gui.App.unregisterGlobalHotKey(shortcut)
				gui.App.registerGlobalHotKey(shortcut);
			});
		});

		win.on('blur', function(){
			options.forEach(function(option){
				// Create a shortcut with |option|.
				var shortcut = new gui.Shortcut(option);

				// Register global desktop shortcut, which can work without focus.
				gui.App.unregisterGlobalHotKey(shortcut)
			});
		});
		
		/**********************************************************/		

		/*********************Scope********************************/
		$scope.document = {
			saved: true
		};

		$scope.clearEditor = function(){
			if (!saveBeforeContinue()) {
				editor.getSession().setValue('');
				$scope.$apply(function(){
					$scope.document.path = '';
					$scope.document.saved = true;
				});
			};
		};

		$scope.chooseFile = function(){
			var chooser = $('#fileDialog');
			chooser.change(function(evt) {
				importMd($(this).val());
				$(this).val('');
			});
			chooser.trigger('click');
		};

		$scope.saveFile = function(data){
			var data = editor.getSession().getValue();
			if ($scope.document.path) {
				fs.writeFile($scope.document.path, data, function(err) {
					if (err) {
						throw err;
					}else {
						$scope.$apply(function(){
							$scope.document.saved = true;
						});
					};
				});
			} else{
				saveAsDialog(data);
			};

		};
		/****************************************************/

		/******************Functions*************************/

		function saveAsDialog (data) {
			var chooser = $('#saveAsDialog');
			chooser.change(function(evt) {
				saveAs(data, $(this).val());
				$(this).val('');
			});
			chooser.trigger('click');
		};


		function saveAs (data, path) {
			fs.writeFile(path, data, function (err) {
				if (err) {
					throw err
				}else {
					$scope.$apply(function(){
						$scope.document.path = path;
						$scope.document.saved = true;
					});
				};
				console.log('It\'s saved!');
			});
		};

		function importMd(path) {
			fs.readFile(path, 'utf8', function (err,data) {
				if (err) {
					$scope.$apply(function(){
						$scope.document.path = '';
					});
					return console.log(err);
				}
				$scope.document.path = path;
				editor.getSession().setValue(data);
				$scope.$apply(function(){
					$scope.document.path = path;
					$scope.document.saved = true;
				});
			});
		};

		function saveBeforeContinue () {
			if ($scope.document.saved) {
				return false;
			} else{
				if( confirm("Current document is not saved. Every change will be lost. Do you want to continue?")) {
					return false;
				} else {
					return true;
				}; 
			};
		};
	}]);

})();